   
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Laporan</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Laporan</a>
                  </li>
                  <li class="breadcrumb-item active">Data Laporan
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <div class="content-header-right col-md-6 col-12 mb-md-0 mb-2">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
              <div class="btn-group" role="group">
                <a class="btn btn-outline-primary mr-2" href="<?php echo base_url(); ?>admin/pembayaran/add"  ><i class="fa fa-plus"></i> Tambah Data </a>
                <a class="btn btn-outline-warning mr-2" href="<?php echo base_url(); ?>admin/report/pdf"  ><i class="fa fa-file"></i> Download PDF </a>
                <a class="btn btn-outline-success" href="<?php echo base_url(); ?>admin/report/excel"  ><i class="far fa-file-excel"></i> Download Excel </a>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
<section id="horizontal">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Laporan</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                     
                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id</th>
                                    <th>Tanggal Booking</th>
                                    <th>Kode Booking</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Telephone</th>
                                    <th>Jam Mulai</th>
                                    <th>Jam Selesai</th>
                                    <th>Total Biaya</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php $a=1; foreach($list as $key => $b) { ?> 
                                <tr>
                                    <td><?php echo $a++; ?></td>
                                    <td><?php echo $b->booking_id; ?></td>
                                    <td><?php echo $b->booking_tanggal; ?></td>
                                    <td><?php echo $b->booking_kode; ?></td>
                                    <td><?php echo $b->booking_nama_pemesan; ?></td>
                                    <td><?php echo $b->booking_email_pemesan; ?></td>
                                    <td><?php echo $b->booking_phone_pemesan; ?></td>
                                    <td><?php echo $b->booking_jam_awal; ?></td>
                                    <td><?php echo $b->booking_jam_akhir; ?></td>
                                    <td><?php echo $b->booking_total_harga; ?></td>
                                    <td><?php echo $b->booking_status; ?></td>
                                </tr>
                              <?php } ?>
                                
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Scroll - horizontal table -->
<!--/ Scroll - horizontal and vertical table -->


        </div>
      </div>
    </div>
    <!-- END: Content-->
    


<pre> 
    <table class="table display nowrap table-striped table-bordered scroll-horizontal"><thead>
        <tr>
            <th>No</th>
            <th>Id</th>
            <th>Tanggal Booking</th>
            <th>Kode Booking</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>Jam Mulai</th>
            <th>Jam Selesai</th>
            <th>Total Biaya</th>
            <th>Status</th>
        </tr>
    </thead><tbody>
        <?php $a=1; 
        foreach ($data as $b): ?> 
            <tr>
                <td><?php echo $a++; ?></td>
                <td><?php echo $b->booking_id ?></td>
                <td><?php echo $b->booking_tanggal ?></td>
                <td><?php echo $b->booking_kode ?></td>
                <td><?php echo $b->booking_nama_pemesan ?></td>
                <td><?php echo $b->booking_email_pemesan ?></td>
                <td><?php echo $b->booking_phone_pemesan ?></td>
                <td><?php echo $b->booking_jam_awal ?></td>
                <td><?php echo $b->booking_jam_akhir ?></td>
                <td><?php echo $b->booking_total_harga ?></td>
                <td><?php echo $b->booking_status ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody></table>
</pre>
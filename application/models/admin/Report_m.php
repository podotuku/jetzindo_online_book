<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_m extends CI_Model {

	public function lists()
 	{
        $this->db->select('booking_id, booking_tanggal, booking_kode, booking_nama_pemesan, booking_email_pemesan, booking_phone_pemesan, booking_jam_awal, booking_jam_akhir, booking_total_harga, booking_status');
 		$this->db->from('booking p');
 		$this->db->order_by('p.booking_id', 'asc');	
	    $query = $this->db->get();
	    if($query->num_rows()>0)
	      {return $query->result();}
	    else{return null;}
 	}
}

/* End of file Pembayaran_m.php */
/* Location: ./application/models/admin/Pembayaran_m.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Report_m','booking');
		$this->simple_login->check_login();
	}

	public function index()
	{
		$data = array('title' => 'Data Pembayaran | Jetz Indonesia',
					  'content' => 'admin/Report/report',
					  'list' => $this->booking->lists()
					 );
		$this->load->view('admin/template/wrapper', $data, FALSE);
	}

	public function pdf(){

		$data['data'] = $this->db->get('booking')->result();

		$this->load->library('pdf');
		$customPaper = 'A4';
		$this->pdf->setPaper($customPaper, 'landscape');
		$this->pdf->load_view('admin/laporan_pdf', $data);
	}

	public function excel(){
		$data = array( 'title' => 'Laporan Excel',
		'list' => $this->booking->lists());
		$this->load->view('admin/report/reporter',$data);
		}
	
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */